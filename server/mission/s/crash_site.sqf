_text = _this select 0;
_target = _this select 1; // Position Array


_veh = [["C130J_wreck_EP1","c130"],["B_Heli_Transport_01_camo_F","uh80"]];

_c = count _veh;
_v = floor (random _c);
_b = _veh select _v;


hint format["%1",_v];
_t = "sm" + str(time);
[_t," Crash Site ","Bergen sie alle Verletzen "] call SHK_Taskmaster_add;

_pos = [_target,[20,100],random 360] call SHK_pos;

_markerstr = [_target] call sidemission_marker;
  
_veh = _b select 0 createVehicle _pos;

_a = 0;
_b = 3;
while {_a <=  _b} do {
    

	_pos = [_target,[25,100],random 360] call SHK_pos;
	_group = createGroup west;
    
	_u = _group createUnit ["BWA3_Rifleman_Tropen", _pos, [], 0, "FORM"];
    _u setcaptive true;
    if(dev)then{
        createMarker [str _pos, _pos] setmarkertype "hd_unknown";
    };
	_u setdamage 0.75;
	_u switchmove "Acts_InjuredLyingRifle02";
    
	server setVariable [ format["bs_join_sm_grp_player_%1",_a],_u];
	[ [_u],"joinGrp","Helfen" ] call fncLib_addActionOption;
    
    _a = _a + 1

    };


[ [_markerstr],[3,2],[7,2],[3,1],[2],[0],[0,0],[5,1,enemySpawnZoneEosDistance,enemySideFactionEos ,TRUE,eosDev] ] call missionCallEos;


/*
 * Setup Global Vars
 */
server setVariable ["bs_sidemission_dac",_dac];

sleep 5;
_u0 = server getVariable "bs_join_sm_grp_player_0";
_u1 = server getVariable "bs_join_sm_grp_player_1";
_u2 = server getVariable "bs_join_sm_grp_player_2";
_u3 = server getVariable "bs_join_sm_grp_player_3";


waitUntil { (position _u0 distance hq < 10) AND (position _u1 distance hq < 10) AND (position _u2 distance hq < 10) AND (position _u3 distance hq < 10) OR sidemission_abort };


if( !sidemission_abort )then{
    [_t,_markerstr] call sidemission_successful;
    
    _u = [_u0,_u1,_u2,_u3];
    {deleteVehicle _x}foreach _u;

}else{
    //Mission abort
   
	_v = server getVariable "bs_sidemission";
    
	if(_v != "noActive")then{
        
        ['bs_join_sm_grp_player'] call fncLib_abortMissionVars;
        
	}else{
        
		[hq_logic_papa, "Keine laufende Mision"] call zp_globalCommandChatClients;
        
    };
    
    
    
}
