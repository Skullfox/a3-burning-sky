//request need mission type, get one if available
    
mission_abort = false;

_arg = [_this, 0, ""] call BIS_fnc_param;

systemChat format["%1",_this select 0];
_req = _this select 0;

sleep 5;

//get all villages on map
_cities = nearestLocations [position Server, ["NameVillage"], 80000];

//select a random one
_target = [_cities] call fncLib_randomArray;

//return village string
_loc =  text _target;

//if already used restart script
if( _loc in used_cities )then{
    
    execVM "server\mission\init_missions.sqf";
    
}else{
    
    //get mission status
    
	_v = server getVariable "bs_mission";
	systemChat format["%1",_v];
       
	if(_v == "noActive")then{
  
  		//add village to array
	    used_cities = used_cities + [_loc];
        
	    if(!isRandomMissions )then{
            
           	systemChat format ["req - %1",_req];
            //if reqest take that
            
	       [_req] call fncLib_startMission;
            
	    }else{
            systemChat "random";
			//select random main mission	
			_mission = [mission_types] call fncLib_randomArray;
            systemChat format ["random - %1",_mission];
            [_mission] call fncLib_startMission;
            
        };
	       
   }else{
       [localize "str_no_mission_available", localize "str_placeholder", localize "str_placeholder"] call zp_missionInfoTextClients;     
   };
};

