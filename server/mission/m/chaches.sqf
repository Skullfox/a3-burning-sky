_text = _this select 0;
_target = _this select 1;

_crate_class ="Box_FIA_Support_F";


[_text,_text,localize "str_destroy_chaches_desc" ] call SHK_Taskmaster_add;
_markerstr = [_target] call mission_marker;
_eosMarkerstr = [_target] call missionEosmarker;
target setpos getMarkerPos _markerstr;

systemChat "start eos";
[false,false,false] call missionCallEos;

[_markerstr] call mission_dummy_unit;

    _pos1 = [ target ] call fncLib_findHousePos;   
	waitUntil {
		if (count _pos1 > 1) exitWith {true};  // has to return true to continue
		sleep 1;
		 _pos1 = [ target ] call fncLib_findHousePos;
	};
    
    _crate1 = createVehicle [_crate_class, _pos1, [], 0, "CAN_COLLIDE"];  
	_crate1 addEventHandler ["killed", {  [hq_logic_papa, "Chache zerstört"] call zp_globalCommandChatAll;}];
    _pos2 = [ target ] call fncLib_findHousePos;   
	waitUntil {
		if (count _pos2 > 1) exitWith {true};  // has to return true to continue
		sleep 1;
		 _pos2 = [ target ] call fncLib_findHousePos;
	};
	_crate2 = createVehicle [_crate_class, _pos2, [], 0, "CAN_COLLIDE"];  
    _crate2 addEventHandler ["killed", {  [hq_logic_papa, "Chache zerstört"] call zp_globalCommandChatAll;}];
	_pos3 = [ target ] call fncLib_findHousePos;   
	waitUntil {
		if (count _pos3 > 1) exitWith {true};  // has to return true to continue
		sleep 1;
		 _pos3 = [ target ] call fncLib_findHousePos;
	};
    _crate3 = createVehicle [_crate_class, _pos3, [], 0, "CAN_COLLIDE"];  
 _crate3 addEventHandler ["killed", {  [hq_logic_papa, "Chache zerstört"] call zp_globalCommandChatAll;}];

systemChat "crates  spawned";
_r = server getVariable "bs_mission";

waitUntil { (!alive _crate1) AND (!alive _crate2) AND (!alive _crate3) OR (mission_abort) };

if( (_text == _r ) AND (!mission_abort))then{
    player sidechat format["%1 - %2",_text,_r];
    [_markerstr,_r] call mission_successful;
	sleep 5;
	execVM "server\mission\standby.sqf";
}else{
    //Mission abort
   
	_v = server getVariable "bs_mission";
    
	if(_v != "noActive")then{
        
    ['marker'] call fncLib_abortMissionVars;
    ['task'] call fncLib_abortMissionVars;

	}else{
        
        [hq_logic_papa, "Keine laufende Mision"] call zp_globalCommandChatClients;
        
    };
    
}