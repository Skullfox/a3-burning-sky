addMarkerToHeli = {
   
	_v = _this select 0;
	_veh = _this select 1;
    
    [_v,_veh] spawn {

	    _v = _this select 0;
	    _veh = _this select 1;
	    
        while{alive _veh} do {
			_v setMarkerPos getpos _veh;
      		 sleep 10;
        };
	    
	};
         
};


startHeliTour = {
    systemchat "startHeliTour";
    _vehicle = _this select 0;
    _startPad = _this select 1;
    _endPad = _this select 2;
    _id = _this select 3;
    
    _vehicle domove getPos _endPad;
    _vehicle flyinheight 120;
	 _vehicle disableAi "TARGET"; 
	_vehicle disableAi "AUTOTARGET"; 
	_vehicle enableAttack false; 
	_vehicle setCombatMode "BLUE"; 
	_vehicle setBehaviour "CARELESS"; 
	_vehicle allowFleeing 0;  
  
    
     [_endPad,_vehicle,_id,_startPad] spawn {

	    _endPad = _this select 0;
	    _vehicle = _this select 1;
	     _id = _this select 2;
         _c = true;
         _startPad = _this select 3;
         
        while{alive _vehicle AND _c} do {
			sleep 2;
			_meters = _vehicle distance _endpad;
            systemchat format ["ID %1: %2m",_id,_meters];
            if(_meters < 200)then{

                _c = false;
               dostop _vehicle;
                systemchat format ["ID %1: landet ",_id];
                _vehicle land "LAND";
                
                [_vehicle,_startPad,_endPad,_id] call launchTime;
                
            };
            
        };
	    
	};
      
};

heliInit = {
  
    _heliArray = ["C_Heli_Light_01_civil_F","RHS_Mi8amt_civilian"];

	_y = 0;
	_heliPads= nearestObjects[position server,["HeliHCivil"],500000];
	/*
    {    
		_m = createMarker [str position _x, position _x];
        _m setMarkerType "mil_flag";
        _m setMarkerText format["Helipad - %1",_y];
        _y = _y + 1;
	} 
	foreach _heliPads;
	*/
    
_c = count  _helipads;
_numbers = floor( _c / 2  );

//hint format ["%1",_numbers];


_linkedHelipads = [];

for [{_i=0}, {_i< _numbers}, {_i=_i+1}] do
{
    
    _index = _i;
    _helipadStart = _helipads select _index;
    _indexEnd = _index + _numbers;
    
   _helipadEnd = _helipads select _indexEnd;
    
    _linkedHelipads pushBack [_helipadStart,_helipadEnd];
    
    
    
};

//hint format["%1",_linkedHelipads];

_y = 0;
    {    
    	_start = _x select 0;
        _end = _x select 1;
        _class = [_heliArray] call selectHeliCiv;        
        _veh = _class createVehicle position _start;
        createVehicleCrew _veh;
        
        if(dev)then{
            
    		_m = createMarker [str position _start, position _start];
	        _m setMarkerType "mil_flag";
	        _m setMarkerText format["Helipad - %1 start",_y];
        
			_v = createMarker [str position _veh, position _veh];
	        _v setMarkerType "n_air";
	        _v setMarkerColor "ColorGreen";
	        _v setMarkerText format["Heli - %1 start",_y];
        
			[_v,_veh] call addMarkerToHeli;
            
            _m = createMarker [str position _end, position _end];
	        _m setMarkerType "mil_flag";
	        _m setMarkerText format["Helipad - %1 end",_y];
        };
       
        _y = _y + 1;
        
        [_veh,_start,_end,_y] call startHeliTour;
        
        
	} 
	foreach _linkedHelipads;  
    
};

launchTime = {
    //[_vehicle,_startPad,_endPad,_id]
    
    _vehicle = _this select 0;
    _startpad = _this select 1;
    _endPad = _this select 2;
    _id = _this select 3;
    
    _timeoutBase =  240;
    _timeOutAdditional = floor (random 300);
    _timeout = _timeoutBase + _timeOutAdditional;
    _vehicle setfuel 1;
    systemChat format ["ID: %1 macht %2s pause.",_id,_timeout];
    sleep _timeout;
    
    if(_vehicle distance _endPad < 50 )then{
			hint "endpad";
            
             systemChat format ["ID: %1 startet wieder.",_id];
    		 [_vehicle,_endPad,_startpad,_id] call startHeliTour;
    }else{
        hint "startpad";
             systemChat format ["ID: %1 startet wieder.",_id];
    		 [_vehicle,_startPad,_endPad,_id] call startHeliTour;
    };
    
    

};

selectHeliCiv = {
    
    _heliArray = _this select 0;
    _c = count _heliArray;
    
    _i = floor( random _c );
    
    _class = _heliArray select _i;
    
    _class
    
    
};