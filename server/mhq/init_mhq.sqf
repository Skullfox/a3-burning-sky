
#include "settings_mhq.sqf"

mhq = mhq_class createVehicle getmarkerPos mhq_spawn;



mhq setDir mhq_dir;

[west, mhq] call BIS_fnc_addRespawnPosition;

systemChat "mhq spawned";
publicVariable "mhq";

_id= mhq addEventHandler ["killed", {execVM "server\mhq\init_mhq.sqf"}];

[mhq,mhq_marker] spawn {
    
    _mhq = _this select 0;
    _mhq_marker = _this select 1;
    
    while{alive _mhq}do{
        
       _mhq_marker setMarkerPos getpos _mhq;
       sleep 10;
    }
    
};