systemChat "Start Server";
//Missions

_missionOrderParam = "missionOrderParam" call BIS_fnc_getParamValue;
_eosSupportTimerParam = "eosSupportTimerParam" call BIS_fnc_getParamValue;

server setvariable ["database",missionName + "_" + worldName,true];
server setvariable ["bs_supportTimer",_eosSupportTimerParam,true];

mission_types = ["chaches","hvt","clean_village"];
sidemission_types = ["crash_site"];

//Array with used main missions
used_cities = ["Gromada","Borosh","Pilana Sawmill"];

//first start init vars
server setVariable ["bs_mission","noActive"];
server setVariable ["bs_sidemission","noActive"];

server setvariable ["bs_mode","hard"];

mission_abort = false;
sidemission_abort = false;
//setup mhq
execVM "server\mhq\init_mhq.sqf";

if(_missionOrderParam == 0)then{
    isRandomMissions = true;
    systemChat "Mission Type Random";
    sleep 20;
	[] execVM "server\mission\init_missions.sqf";
    
}else{
    isRandomMissions = false;
    systemChat "Mission Type Request";
    
};

publicVariable "server";