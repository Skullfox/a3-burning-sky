zp_clientLibShoutCivDown = {
    private ["_player"];
    
    _player = _this select 0;
    
	_nearCivs = nearestobjects [_player,["Man"],10];
   
    hintsilent format["Legen Sie sich auf den Boden !"];
    {
		if ((side _x) == civilian AND ( !isPlayer _x)) then {    
			//doStop _x;
        	_x playMove "amovppnemstpsnonwnondnon";
            //_x switchMove "amovppnemstpsnonwnondnon";
            _x disableAI "ANIM";
			//_x setUnitPos "down";
                           
		};    
    }foreach _nearCivs;
    
};


zp_clientLibShoutCivUp = {
    private ["_player"];
    
    _player = _this select 0;
    
	_nearCivs = nearestobjects [_player,["Man"],10];
   
    hintsilent format["Aufstehen !"];
    {
		if ((side _x) == civilian AND ( !isPlayer _x)) then {    
			//doStop _x;
            //_x switchMove "amovppnemstpsnonwnondnon";
            _x enableAI "ANIM";
			_x setUnitPos "up";
                           
		};    
    }foreach _nearCivs;
    
};


saveAll = {
    
    _db = server getVariable "database";
    
     _pdw = ["new", "profile"] call OO_PDW;
     
	["setFileName", _db] call _pdw;

	["savePlayer", player] call _pdw;
	["saveInventory", [name player, player]] call _pdw;

};


loadAll = {
    
    _db = server getVariable "database";
    
     _pdw = ["new", "profile"] call OO_PDW;
     
	["setFileName", _db] call _pdw;

	["clearInventory", player] call _pdw;

	["loadPlayer", player] call _pdw;
	["loadInventory", [name player, player]] call _pdw;
};

