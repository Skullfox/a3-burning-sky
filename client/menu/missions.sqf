

_hq_menu = ['hq_menu','Hauptquartier','',{},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _hq_menu] call ace_interact_menu_fnc_addActionToObject;


_abort = ['abort','Mission abbrechen','',{createDialog "missionDialog"; },{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions","hq_menu"], _abort] call ace_interact_menu_fnc_addActionToObject;

//_random = ['random','Zufalls Missions','',{[] call zp_requestMissionServer;},{true}] call ace_interact_menu_fnc_createAction;
//[player, 1, ["ACE_SelfActions", "hq_menu"], _random] call ace_interact_menu_fnc_addActionToObject;

_clean_village = ['clean_village','Stadt säubern','',{["clean_village"] call zp_requestMissionServer},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "hq_menu"], _clean_village] call ace_interact_menu_fnc_addActionToObject;


_chaches = ['chaches','Waffenchaches zerstören','',{["chaches"] call zp_requestMissionServer},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "hq_menu"], _chaches] call ace_interact_menu_fnc_addActionToObject;


_hvt = ['hvt','HVT und Stadt säubern','',{["hvt"] call zp_requestMissionServer},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "hq_menu"], _hvt] call ace_interact_menu_fnc_addActionToObject;

if(dev)then{
_sm = ['sidemission','Sidemission','',{["crash_site"] call zp_requestSideMissionServer},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "hq_menu"], _sm] call ace_interact_menu_fnc_addActionToObject;
};



    
/*
_missions = ['missions','Missionen','',{},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _menu] call ace_interact_menu_fnc_addActionToObject;


_random_mission = ['random_mission','Zufalls Missions','',{[] call zp_requestMissionServer;},{true}] call ace_interact_menu_fnc_createAction;
_clean_village = ['clean_village','Stadt säubern','',{["clean_village"] call zp_requestMissionServer;},{true}] call ace_interact_menu_fnc_createAction;
_chaches = ['chaches','Chaches zerstören','',{["chaches"] call zp_requestMissionServer;},{true}] call ace_interact_menu_fnc_createAction;
_hvt = ['hvt','HVT Ziel','',{["hvt"] call zp_requestMissionServer;},{true}] call ace_interact_menu_fnc_createAction;
   
     

[player, 1, ["ACE_SelfActions", "missions"], _random] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "missions"], _neutralMenu] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "missions"], _teleMenu] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "missions"], _neutralMenu] call ace_interact_menu_fnc_addActionToObject;

*/