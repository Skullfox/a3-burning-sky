call compile preprocessFileLineNumbers "lib\ascom.sqf";
call compile preprocessfilelinenumbers "lib\shk_taskmaster.sqf";
call compile preprocessfilelinenumbers "shk_pos\shk_pos_init.sqf";
call compilefinal preprocessFileLineNumbers "oo_pdw.sqf";
call compile preprocessfilelinenumbers "server\ambientHeli.sqf";

showChat false;
#include "settings.sqf";

_modules = [
	["pxs_satcom_a3",0,true],
    ["eosUnits",1,true],
	["cos",1,true],
   	["bonIeds",1,true],
   	["ambientHeli",1,true]
];

[_modules] execVM "lib\loader.sqf";

[]execVM "eos\OpenMe.sqf";
execVM "lib\dev.sqf";
execVM "client\clientLib.sqf";
execVM "lib\missionFunctions.sqf";
execVM "R3F_LOG\init.sqf";

if (hasInterface) then {
	execVM "client\client.sqf";
};


