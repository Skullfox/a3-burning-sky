/*
 * 
 *  AWESOME SUICIDE SCRIPT
 * 
 *  MADE BY PHIL | FREE TO MODIFY
 * 	DO NOT REMOVE LINK
 * 	https://github.com/prowox/
 * 
 */
 
_isDev = false; //false

private ["_player","_suicidebomber","_playerarray","_nmax","_select"];

_civ = _this select 0;



_explosion = {
	private ["_civ","_bomb"];
	_civ = _this select 0;
	_bomb = _this select 1;
	_vest = _this select 1;
    sleep 0.5;
	_bomb createVehicle getPos _civ;
    _bomb createVehicle getPos _civ;
};

if (!isServer) exitWith {};

	_suicidebomber = _civ;



	waitUntil { (_suicidebomber distance player  <= 30) OR ( _suicidebomber distance vehicle player  <= 30 )};

	//Player Classes Change
	_playerarray = nearestObjects [_suicidebomber, [
	"BWA3_TL_Tropen",
	"US_Soldier_EP1",
	"US_Soldier_Medic_EP1",
	"US_Soldier_Engineer_EP1",
	"US_Delta_Force_TL_EP1",
	"US_Delta_Force_Assault_EP1",
    "CAR"
	], 35];

    systemChat format["%1",_playerarray];
    
_ex1 = "DemoCharge_Remote_Ammo" createVehicle position player;
_ex1 attachTo [_civ, [-0.1, 0.1, 0.15], "Pelvis"];
_ex1 setVectorDirAndUp [ [0.5, 0.5, 0], [-0.5, 0.5, 0] ];
_ex2 = "DemoCharge_Remote_Ammo" createVehicle position player;
_ex2 attachTo [_civ, [0, 0.15, 0.15], "Pelvis"];
_ex2 setVectorDirAndUp [ [1, 0, 0], [0, 1, 0] ];
_ex3 = "DemoCharge_Remote_Ammo" createVehicle position player;
_ex3 attachTo [_civ, [0.1, 0.1, 0.15], "Pelvis"];
_ex3 setVectorDirAndUp [ [0.5, -0.5, 0], [0.5, 0.5, 0] ]; 
	
	_nmax = count _playerarray;
	_select =floor(random _nmax);
	_player = (_playerarray select _select);
    
	systemChat format["%1 - %2 ",_player,vehicle _player];
        
	//Sound
	_suicidebomber say3D "suidice_bomber";
	
	//Debug
    if(_isDev)then{hint format["INCOMING for you %1!", _player];};
	_suicidebomber SetSpeedMode "Full";
	//_suicidebomber SetCombatMode "Red";
	_suicidebomber SetBehaviour "Careless";
	//_suicidebomber disableAI "move";
	//_suicidebomber domove getpos _player;

	_group = createGroup civilian;
	_suicidebomber joinAsSilent [_group, 4];
	_group addWaypoint [position _player, 0];
    

/*
while {_suicidebomber distance _player  > 25;} do {
sleep 1;
_group addWaypoint [position _player, 0];

 if(_isDev)then{hint"new wp";}

};
*/


waitUntil { _suicidebomber distance _player  < 10 };

if ( alive _suicidebomber ) then {
		
		
        [_suicidebomber,"M_Mo_120mm_AT"] call _explosion;
        
        {
		deleteVehicle _x;
		} forEach [_ex1,_ex2,_ex3];  



        //_nade1 = "GrenadeHand" createVehicle getPos _suicidebomber;

        //_nade attachTo[_suicidebomber,[0,0,1]];
        //_nade1 attachTo[_suicidebomber,[0,0,1]];
        //_nade2 attachTo[_suicidebomber,[0,0,1]];
		//"GrenadeHand"
		//"Sh_122_HE"
		//"Bo_GBU12_LGB_MI10"
	
	}else{
	
    	 if(_isDev)then{hint "no bomb anymore";};

};
