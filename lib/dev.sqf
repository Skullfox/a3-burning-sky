// Executed on the server.
zp_loadDevSettings = {
    private ["_param0"];

    _param0 = _this select 0;

	_settings = [clientModulesActive,serverModulesActive];
    
	_settings
};


zp_missionInfoText = {
    private ["_param0", "_param1", "_param2", "_param3"];

    _param0 = _this select 0;
    _param1 = _this select 1;
    _param2 = _this select 2;
    _param3 = _this select 3;
 	sleep 1;
    [
        [
                [_param0,"<t align = 'center' size = '0.7'>%1</t><br/>"],
                [_param1,"<t align = 'center' size = '0.7'>%1</t><br/>"],
                [str (date select 3) + ":" + str (date select 4),"<t align = 'center' size = '0.7'>%1</t>"]
 
        ]
	] spawn BIS_fnc_typeText;
    
   
};
    
sidemission_marker = {
    
        _target = _this select 0;
    
    _markerstr = createMarker ["smtarget_" +  str(time), _target ];
	_markerstr setMarkerShape "ICON";
	_markerstr setMarkerColor "ColorRed";
	_markerstr setMarkerType "waypoint";

   	server setVariable ["bs_sidemission_marker",_markerstr];
	publicVariable "server";
    
	_markerstr
    
};

sidemission_successful = {
    
    _t = _this select 0;
	_m = _this select 1;
    
    deleteMarker _m;
    
	[_t,"succeeded"] call SHK_Taskmaster_upd;	 
    server setVariable ["bs_sidemission","noActive"];
    publicVariable "server";
    
};

fncLib_addActionOption = {
  
  	_array = _this select 0;
    _sqf = _this select 1;
	_name = _this select 2;

	{
       _x addAction [_name,format["client\actions\%1.sqf",_sqf] ];
       
    }foreach _array;
    

};

fncLib_abortMissionVars = {
    
    _typ = _this select 0;
    
    switch (_typ) do {
        
    case 'marker': { 
			        	_d = server getVariable "bs_mission_marker";
						_d setMarkerColor "ColorBlue";
                        server setVariable ["bs_mission_marker",false];
					};
                    
    case 'trigger': {
           				_f = server getVariable "bs_mission_trigger";
    					deletevehicle _f;
                        server setVariable ["bs_mission_trigger",false];
					};
                    
    case 'support': {
						_a = server getVariable "bs_mission_support_object";
						_a setDamage 1;
						server setVariable ["bs_mission_support_object",false];
					};
                    
    case 'task': {
        				_v = server getVariable "bs_mission";
						[_v,"failed"] call SHK_Taskmaster_upd;
						server setVariable ["bs_mission","noActive"];
						[hq_logic_papa, "Mission wurde abgebrochen"] call zp_globalCommandChatClients;
					};
                    
    case 'bs_join_sm_grp_player': {
        
        server setVariable ["bs_join_sm_grp_player_0",false];
		server getVariable ["bs_join_sm_grp_player_1",false];
		server getVariable ["bs_join_sm_grp_player_2",false];
		server getVariable ["bs_join_sm_grp_player_3",false];
        server getVariable ["bs_join_sm_grp_player_4",false];

					};                                                                                                                                                 
             
    default { hint "fncLib_abortMissionVars no params" };
    
	};
    systemChat "cleaned up";
    publicVariable "server";
};



fncLib_randomArray = {
    
    _array = _this select 0;
    
    _c = count _array;
	_n = floor ( random  _c );
	_i = _array select _n;

    _i
};

// Executed on the server.
zp_requestSideMission = {
    private ["_param1", "_param2", "_param3", "_param4"];

   _param1 = _this select 0;
      
    [hq_logic_papa, "Neue SMission wird angefordert"] call zp_globalCommandChatClients;
    [_param1] execVM "server\mission\init_sidemission.sqf";
};

fncLib_stratMapPort = {
	_m = _this select 0;
    player setpos getmarkerPos _m;
};


mission_trigger = {
    
     _target = _this select 0;
    
    _enemy = "resistance";
    
    systemChat "setup trigger";
    
    kt = str( floor( time ) ) + "_" + str ( floor( random( 999 ) ) );
	kb = 'birdistheword'; 
    
	_trigger = createTrigger ["EmptyDetector", position _target ];
	_trigger setTriggerArea [400, 400, 0, true];
	_trigger setTriggerActivation [enemyTriggerStr, "NOT PRESENT", true];
	_trigger setTriggerStatements ["this",format["kb = '%1'",kt], ""];
    
	server setVariable ["bs_mission_trigger",_trigger];
	server setVariable ["bs_mission_cleaned",kt];

};

mission_dummy_unit = {
    
    enemyDummyUnit = server getvariable "enemyDummyUnit";
    
    _position_truck = getMarkerPos _markerstr findEmptyPosition [10,350,"O_Truck_02_covered_F"];
	waitUntil {
		
		if (count _position_truck > 1) exitWith {true};  // has to return true to continue
		sleep 1;
		_position_truck = getMarkerPos _markerstr findEmptyPosition [10,350,"O_Truck_02_covered_F"];  
	};

	_vehicle = createVehicle [enemyDummyUnit , [ _position_truck select 0,  _position_truck select 1], [], 0, ""];
	createVehicleCrew _vehicle;     
    systemChat "Dummy unit done";
};


mission_enemy_support = {
    
    _markerstr = _this select 0;
	_target = _this select 1;
    _hq_logic_papa = _this select 1;
    
    _position_antenna = getMarkerPos _markerstr findEmptyPosition [10,350,"PowGen_Big"];
	waitUntil {
	    
	  // exit loop if the unit gets deleted
		if (count _position_antenna > 1) exitWith {true};  // has to return true to continue
		sleep 1;
		_position_antenna = getMarkerPos _markerstr findEmptyPosition [10,350,"PowGen_Big"];
	};
	
    /*
	_ant_marker = createMarker ["antenna_"+  str(time), [ _position_antenna select 0, _position_antenna select 1]];
	_ant_marker setMarkerShape "ICON";
	_ant_marker setMarkerType "loc_Transmitter";
	_ant_marker setMarkerColor "ColorRed";
	*/
    
	_land_antenna = createVehicle ["PowGen_Big" , [  _position_antenna select 0,  _position_antenna select 1], [], 0, ""];
    _land_antenna addEventHandler ["killed", {  [hq_logic_papa, "Keine Feindlichen funkübertragungen mehr im Gebiet"] call zp_globalCommandChatClients;}];
	
    _land_antenna1 = createVehicle ["Land_antenna" , [  (_position_antenna select 0) + 3, ( _position_antenna select 1) + 3 ], [], 0, ""];
    _land_antenna1 setDir random 360;
    
    
    server setVariable ["bs_mission_support_object",_land_antenna];
    
	systemChat "antenna  done";
	
    _timer = server getVariable "bs_supportTimer";
    
	[_land_antenna,_target,_hq_logic_papa,_timer] spawn {
	    
	    _land_antenna_spawn = _this select 0;
	    _paras = _this select 1;
		_hq = _this select 2;
        _timer = _this select 3;
	 	systemChat "paras waiting";
	    
	    while { damage _land_antenna_spawn < 0.8 } do {
	        
			sleep _timer;//every 13 min paras incomming
	    	[_paras] execVM "lib\reinforcements.sqf"
	    
		};  
	 };
};



mission_successful = {
   
    _markerstr = _this select 0;
	_text = _this select 1;

	_markerstr setMarkerColor "ColorGreen";
	[_text,"succeeded"] call SHK_Taskmaster_upd;	 
    server setVariable ["bs_mission","noActive"];
    publicVariable "server";
};


mission_marker = {
    
    _target = _this select 0;
    
    _markerstr = createMarker ["target_" +  str(time),[ position _target select 0, position _target select 1] ];
	_markerstr setMarkerShape "ELLIPSE";
	_markerstr setMarkerBrush "FDiagonal";
	_markerstr setMarkerSize [400,400];
	_markerstr setMarkerColor "ColorRed";

	server setVariable ["bs_mission_marker",_markerstr];

	_markerstr
    
};



// Executed on the server.
zp_abortMission = {
    
	private ["_param1", "_param2", "_param3", "_param4"];
    mission_abort = true;
};

fncLib_carRadio = {
    
    _car = _this select 0;
    _player = _this select 1;
    _status = _this select 2;
    
	hint"Radio ist kaputt";
      
};

fncLib_findHousePos = {
 
_target = _this select 0;

_nearesthouses = position target nearObjects ["House",350];
_houseList = [];
{
    for "_i" from 0 to 20 do {
        if ( [(_x buildingPos _i), [0,0,0]] call BIS_fnc_arrayCompare ) exitWith {
            if (_i > 0) then {
                _houseList set [count _houseList, [_x, _i]];
            };
        };
    };    
}forEach _nearesthouses;

_randomHouse = _houseList select (floor (random (count _houseList)));
_housePos = (_randomHouse select 0) buildingPos (floor (random (_randomHouse select 1)));

if( dev )then{
    
_crate_marker = createMarker ["aas_" + str( time ), _housePos];
_crate_marker setMarkerShape "ICON";
_crate_marker setMarkerType "loc_Tree";
_crate_marker setMarkerColor "ColorRed";

};

_housePos
    
};


fnclib_chatDump = {

		_str = _this select 0;
		player sidechat _str;

};

fncLib_teleport = {
    
    onMapSingleClick "player setPos _pos; onMapSingleClick ''"; 
    
};

fncLib_airstrike = {
        
};

zp_requestMission = {
	private ["_param1", "_param2", "_param3", "_param4"];
    
   _param1 = _this select 0;
      
    [hq_logic_papa, "Neue Mission wird angefordert"] call zp_globalCommandChatClients;
    [_param1] execVM "server\mission\init_missions.sqf";
       	
};


fncLib_captive = {
    
   player setcaptive true;
    
};


zp_globalCommandChat = {
    private ["_caller", "_text"];

    _caller = _this select 0;
    _text = _this select 1;

   _caller commandChat _text;
   
};


fnclib_findPos = {

if (!isServer) exitWith {};

_NameVillage = nearestLocations [position Server, ["NameVillage"], 100000];
_possibleLocation = [];
_blacklist  = [];

{
         		
	_newmarker = createMarker [str(position _x),position _x];
	_newmarker setMarkerShape "ICON";
	_newmarker setMarkerText str(_number);
	_newmarker setMarkerColor "ColorYellow";
	_newmarker setMarkerType "mil_objective";
            
            //Mögliche positionen
            _possibleLocation = _possibleLocation + [_x];
            //aus schleife entfernen
   			_NameVillage = _NameVillage - [_x];    

    
}foreach _NameVillage;


_NameCityCapital = nearestLocations [position Server, ["NameCityCapital"], 100000];
_possibleLocation = [];
_blacklist  = [];

{
         		
	_newmarker = createMarker [str(position _x),position _x];
	_newmarker setMarkerShape "ICON";
	_newmarker setMarkerText str(_number);
	_newmarker setMarkerColor "ColorGreen";
	_newmarker setMarkerType "mil_objective";
            
            //Mögliche positionen
            _possibleLocation = _possibleLocation + [_x];
            //aus schleife entfernen
   			_NameCityCapital = _NameCityCapital - [_x];    

    
}foreach _NameCityCapital;

_NameCity = nearestLocations [position Server, ["NameCity"], 100000];
_possibleLocation = [];
_blacklist  = [];

{
         		
	_newmarker = createMarker [str(position _x),position _x];
	_newmarker setMarkerShape "ICON";
	_newmarker setMarkerText str(_number);
	_newmarker setMarkerColor "ColorBlue";
	_newmarker setMarkerType "mil_objective";
            
            //Mögliche positionen
            _possibleLocation = _possibleLocation + [_x];
            //aus schleife entfernen
   			_NameCity = _NameCity - [_x];    

    
}foreach _NameCity;


}