
private ["_possibleied","_obj","_man","_eng","_playertype"];
_obj = _this select 0;
_man = _this select 1;

_playertype = typeOf _man;
    
titleText ["Objekt wird untersucht","PLAIN DOWN"]; titleFadeOut 4;

_move = "Acts_carFixingWheel";    
_man playMove _move;

_iedR = ["1","2","3"];

_possibleied = _iedR select (floor(random(count _iedR) ) );   
    
switch (_possibleied) do
 {
     case "1":
     {
         sleep 20;
         _man switchMove "";
		_obj setvariable ["bon_ied_blowit",false,true];
		titleText ["Nur Müll","PLAIN DOWN"]; titleFadeOut 4;
		deleteVehicle _obj;
     };

     case "2":
     {
         sleep 20;
		_man switchMove "";
		_obj setvariable ["bon_ied_blowit",false,true];
		titleText ["IED entschärft","PLAIN DOWN"]; titleFadeOut 4;
		deleteVehicle _obj;
     };  
  
     case "3":
     {
		sleep 20;
          _man switchMove ""; 
         playSound "bs_c4";
         sleep 5;
         "M_Mo_120mm_AT" createVehicle getPos _obj;
         _obj setvariable ["bon_ied_blowit",true,true];
         deleteVehicle _obj;
     };
};    
    


