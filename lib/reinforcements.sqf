_players = call CBA_fnc_players;

if( count _players == 0) exitWith { systemChat "no players online"};

_targetpos = _this select 0;
_exit = "exit";

//move marker

_x = (position _targetpos select 0 ) + 4000;
_y = (position _targetpos select 1 ) + 4000;

_xe = (position _targetpos select 0 ) - 4000;
_ye = (position _targetpos select 1 ) - 4000;

"heli_spawn" setMarkerPos [_x,_y];
"exit" setMarkerPos [_xe,_ye];


_vehicle = createVehicle [enemySupportHeli , getmarkerPos "heli_spawn", [], 0, "FLY"];
_vehicle setpos [(getpos _vehicle select 0),(getpos _vehicle select 1),500];
createVehicleCrew _vehicle; 
systemChat format["%1",enemySupportParas];
_grp_soldier = [getmarkerPos "heli_spawn", enemySide, enemySupportParas ] call BIS_fnc_spawnGroup; 

_group = createGroup resistance;
[_vehicle] join ( _group);
{_x assignasCargo _vehicle;_x moveinCargo _vehicle} forEach units _grp_soldier;

_vehicle setspeedmode "full";
_vehicle domove getpos _targetpos;
_vehicle flyinheight 500;
waituntil {(getPos _targetpos distance _vehicle) < 800 }; //change var1 for detect distance 
sleep 5;
[vehicle _vehicle] execVM "lib\eject_from_plane.sqf";
_smoke = "SmokeShellRed" createVehicle position _vehicle;

sleep 30;
_vehicle domove getMarkerpos _exit;
_wp = _grp_soldier addWaypoint [position _targetpos, 30];
_wp setWaypointType "MOVE";
_wp setwaypointBehaviour "COMBAT";


sleep 60;

{ deleteVehicle _x } forEach (crew _vehicle); deleteVehicle _vehicle;
