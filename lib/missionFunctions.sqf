missionCallEos = {
    
    _eosArray = _this select 0; 
	_param0 = _this select 1; 
	_param1 = _this select 2; 
    
    if(!_eosArray)then{
        [ [_eosMarkerstr],[3,3],[7,3],[3,1],[2],[0],[0,0],[5,1,enemySpawnZoneEosDistance,enemySideFactionEos ,TRUE,eosDev]] call EOS_Spawn;
    }else{
         _eosArray call EOS_Spawn;
    }
  
};

missionEosmarker = {
    
    _target = _this select 0;
    
    _markerstr = createMarker ["targeteos_" +  str(time),[ position _target select 0, position _target select 1] ];
	_markerstr setMarkerShape "ELLIPSE";
	_markerstr setMarkerBrush "FDiagonal";
	_markerstr setMarkerSize enemyEosMarkerSize;
	_markerstr setMarkerColor "ColorRed";

	server setVariable ["bs_mission_eosmarker",_markerstr];

	_markerstr
        
};

fncLib_startMission = {
    
    	_mission = _this select 0;
       
        //format string and start mission file                        
		_file = Format ["server\mission\m\%1.sqf",_mission];
		server setVariable ["bs_mission",_loc]; 
		publicVariable "server";
	   	systemchat _file;
        //skipTime random 4;
        sleep 1;
		//[str _loc, "placeholder", ""] call zp_missionInfoTextClients;                
        //sleep 10;
		[_loc,_target] execVM _file;

};

zp_changeDifficult = {
    
    private ["_param1"];

    _param1 = _this select 0;

    bs_mode = server getvariable "bs_mode";
        	
	if( _dif == "easy")then{
        bs_mode = "hard";
    };
    
	if( _dif == "hard")then{
        bs_mode = "easy";
    };
    
    server setvariable ["bs_mode",bs_mode,true];
                
    bs_mode
    
};
