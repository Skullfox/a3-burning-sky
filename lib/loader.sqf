
	systemchat "Load Modules";
	_modules = _this select 0;
	serverModules = [];
	clientModules = [];
	inactiveModules = [];
	
    serverModulesActive = [];
    clientModulesActive = [];
	{
		_name = _x select 0;
		_whereToExecute = _x select 1;
		_active = _x select 2;
		
			if(_active)then{
		
			_file = format["modules\%1.sqf",_name];
	
			switch (_whereToExecute) do {
				case 0: { 
					clientModules pushBack _name;
					if (hasInterface) then {
						[_name] execVM _file;
					};		
				};
			
				case 1: { 
					serverModules pushBack _name;
					if (isServer) then {
						[_name] execVM _file;
					};		
				};
						
			default { 
				diag_log format["Modul %1 konnte nicht geladen werden !",_name] };
			};
		
		}else{
			inactiveModules pushBack _name;
		}
	
	}foreach _modules;

